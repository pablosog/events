package es.uniovi.imovil.jcgranda.events;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;

public class Alarmas extends BroadcastReceiver {
    private int contador;
    public Alarmas() {
    }
    SharedPreferences prefs;
    private static final String PREFERENCES =  "preferences";
    @Override
    public void onReceive(Context context, Intent intent) {
        // TODO: This method is called when the BroadcastReceiver is receiving
        // an Intent broadcast.
        String dw = intent.getExtras().getString("curso");
        contador = intent.getExtras().getInt("contador");
        prefs =context.getSharedPreferences(PREFERENCES, Context.MODE_PRIVATE);
        int hora = prefs.getInt("hora", 0);
        int minuto = prefs.getInt("minuto", 0);
        triggerNotification(dw, context.getString(R.string.notification)+" "+ hora+":"+minuto, context);
    }
    //Función para sacar la notificación
    public void triggerNotification(String event, String s, Context con){

        Intent intent = new Intent(con, MainActivity.class);
        NotificationManager notificationManager;
        notificationManager = (NotificationManager) con.getSystemService(Context.NOTIFICATION_SERVICE);
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(con)
                        .setSmallIcon(R.drawable.ic_launcher)
                        .setContentTitle(event)
                        .setContentText(s);

        Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        mBuilder.setVibrate(new long[] { 1000, 1000, 1000, 0, 1000 });
        mBuilder.setSound(alarmSound);
        PendingIntent pendingIntent = PendingIntent.getActivity(con, 0,
                intent, 0);
        mBuilder.setContentIntent(pendingIntent);
        notificationManager.notify(contador, mBuilder.build());
    }
}
