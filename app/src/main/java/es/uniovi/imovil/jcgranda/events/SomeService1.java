package es.uniovi.imovil.jcgranda.events;

import android.annotation.SuppressLint;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.TypedArray;
import android.os.IBinder;
import android.widget.Toast;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.TimeZone;

public class SomeService1 extends Service {
    private PendingIntent alarmIntent;
    SharedPreferences prefs;
    private static final String PREFERENCES =  "preferences";
    public SomeService1() {
    }

    @Override
    public void onCreate() {
        super.onCreate();
        prefs =getSharedPreferences(PREFERENCES, Context.MODE_PRIVATE);

    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        // TODO Auto-generated method stub
        if (intent == null){
            return START_STICKY;
        }
        DataWrapper dw = (DataWrapper) intent.getSerializableExtra("lstContact");
        ArrayList<Event> cursoarray = dw.getParliaments();
        Iterator<Event> iterador = cursoarray.iterator();
        int contador = 0;
        while (iterador.hasNext()) {
            Event c = iterador.next();
            int hora = prefs.getInt("hora", 0);
            int minutos=prefs.getInt("minuto", 0);
            int tiempo = (minutos*60+hora*60*60)*1000;
                //Por si se ha saltado algún día y no saltar notificacion al princpio
                while (c.getC().getTimeInMillis()-Calendar.getInstance().getTimeInMillis()-tiempo < 0 && !c.isEspecial() ){

                    Calendar nuevo = c.getC();
                    nuevo.add(Calendar.HOUR, c.getRep());
                    c.setC(nuevo);
                }
                AlarmManager alarmMgr = (AlarmManager)this.getSystemService(Context.ALARM_SERVICE);
                Intent intent2 = new Intent(this, Alarmas.class);
                intent2.putExtra("curso", c.getName());
                intent2.putExtra("contador", contador);
            if (!c.isEspecial())
                alarmIntent = PendingIntent.getBroadcast(this, c.getId(), intent2, PendingIntent.FLAG_CANCEL_CURRENT);

            contador++;
            if (c.isFavorito() && !c.isEspecial() && EventListFragment.activo) {

               alarmMgr.setRepeating(AlarmManager.RTC_WAKEUP, c.getC().getTimeInMillis()-tiempo,
                        1000 * 60 * 60 * c.getRep(), alarmIntent);

               }
            else if (c.isEspecial()){
                Calendar nuevo = new GregorianCalendar();
                nuevo.setTimeZone(TimeZone.getTimeZone("CET"));
                nuevo.set(Calendar.HOUR_OF_DAY, c.getPrimera().get(Calendar.HOUR));
                nuevo.set(Calendar.MINUTE, 0);
                nuevo.set(Calendar.SECOND,0);
                @SuppressLint("Recycle") TypedArray repes_typed = this.getResources().obtainTypedArray(R.array.repe_especiales);
                float [] repes = new float[repes_typed.length()];

                for (int k=0;k<repes.length;k++){
                    repes[k]=repes_typed.getFloat(k,0);
                }
                //modulo?
                for (int i=0; i<repes.length;i++){
                    if (repes[i] == 4.5) {
                        Calendar n = Calendar.getInstance();
                        n.setTimeZone(TimeZone.getTimeZone("CET"));
                        n.set(Calendar.HOUR_OF_DAY, nuevo.get(Calendar.HOUR_OF_DAY));
                        n.set(Calendar.MINUTE, nuevo.get(Calendar.MINUTE));
                        n.set(Calendar.SECOND,nuevo.get(Calendar.SECOND));
                        n.set(Calendar.MILLISECOND,nuevo.get(Calendar.MILLISECOND));
                        alarmIntent = PendingIntent.getBroadcast(this, c.getId()*1000+i, intent2, PendingIntent.FLAG_CANCEL_CURRENT);
                        if (c.isFavorito() && EventListFragment.activo) {

                            if(n.getTimeInMillis()-Calendar.getInstance(TimeZone.getTimeZone("CET")).getTimeInMillis()-tiempo < 0) {
                                n.add(Calendar.HOUR_OF_DAY, 24);
                            }
                           alarmMgr.setRepeating(AlarmManager.RTC_WAKEUP, n.getTimeInMillis() - tiempo,
                                        86400000, alarmIntent);


                        }
                        else {
                            alarmMgr.cancel(alarmIntent);
                        }
                        nuevo.add(Calendar.HOUR_OF_DAY, 4);
                        nuevo.add(Calendar.MINUTE, 30);

                    }
                    else {
                        Calendar n = (Calendar)nuevo.clone();
                        n.setTimeZone(TimeZone.getTimeZone("CET"));
                        alarmIntent = PendingIntent.getBroadcast(this, c.getId()*1000+i, intent2, PendingIntent.FLAG_CANCEL_CURRENT);

                        if (c.isFavorito() && EventListFragment.activo) {

                            if(n.getTimeInMillis()-Calendar.getInstance(TimeZone.getTimeZone("CET")).getTimeInMillis()-tiempo < 0) {
                                n.add(Calendar.HOUR_OF_DAY, 24);
                            }
                            alarmMgr.setRepeating(AlarmManager.RTC_WAKEUP, n.getTimeInMillis() - tiempo,
                                    86400000, alarmIntent);
                        }
                        else
                            alarmMgr.cancel(alarmIntent);
                        nuevo.add(Calendar.HOUR_OF_DAY, (int) repes[i]);

                    }
                }
            }
            else{
                alarmMgr.cancel(alarmIntent);


            }
        }
           stopSelf();
        return START_REDELIVER_INTENT;
    }


    @Override
    public void onDestroy() {
        // TODO Auto-generated method stub
        super.onDestroy();
        Toast.makeText(this, getString(R.string.alarm), Toast.LENGTH_SHORT).show();
    }




}
