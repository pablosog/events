package es.uniovi.imovil.jcgranda.events;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.TimeZone;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

public class EventAdapter extends BaseAdapter {

    /*
    Actualiza la información del listview cuando es llamado, necesario que sea llamado cada segundo para actualizar los contadores.
    Se encarga también de ordenar la lista para que aparezcan primero los evento favoritos y los que más cerca estén de acabar
     */
    @Override
    public void notifyDataSetChanged() {

        int i, j;
        Event aux;
        for(i=0;i< mEvents.size()-1;i++)
            for(j=0;j< mEvents.size()-i-1;j++)
                if(mEvents.get(j).getC().getTimeInMillis()> mEvents.get(j+1).getC().getTimeInMillis()){
                    aux= mEvents.get(j+1);
                    mEvents.set(j+1, mEvents.get(j));
                    mEvents.set(j, aux);
                }
        for(i=0;i< mEvents.size()-1;i++)
            for(j=0;j< mEvents.size()-i-1;j++)
                if(!mEvents.get(j).isFavorito() && mEvents.get(j+1).isFavorito()){
                    aux= mEvents.get(j+1);
                    mEvents.set(j+1, mEvents.get(j));
                    mEvents.set(j, aux);
                }

        super.notifyDataSetChanged();
    }

    static class ViewHolder {
		public TextView mEventName;
        public TextView mContador;
        public TextView mNivel;
        public ImageView mImagen;
        public ImageButton mFavorito;
        public TextView mZona;
	}
	private final ArrayList<Event> mEvents;
	public LayoutInflater mInflater;
    private Context con;
	public EventAdapter(Context context, ArrayList<Event> events) {

		if (context == null || events == null ) {
			throw new IllegalArgumentException();
		}
			con=context;
		this.mEvents = events;
		this.mInflater = LayoutInflater.from(context);

	}
		
	@Override
	public int getCount() {

		return mEvents.size();
	}

	@Override
	public Object getItem(int position) {
		
		return mEvents.get(position);
	}

	@Override
	public long getItemId(int position) {

		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		
		View rowView = convertView;
		final ViewHolder viewHolder;
		if (rowView == null) {
			rowView = mInflater.inflate(R.layout.list_item_events, parent, false);
			viewHolder = new ViewHolder();
			viewHolder.mEventName = (TextView) rowView.findViewById(R.id.nameTextView);
            viewHolder.mContador = (TextView) rowView.findViewById(R.id.contador);
            viewHolder.mFavorito = (ImageButton) rowView.findViewById(R.id.favorite);
            viewHolder.mNivel = (TextView) rowView.findViewById(R.id.nivel);
            viewHolder.mImagen = (ImageView) rowView.findViewById(R.id.imagen);
            viewHolder.mZona = (TextView) rowView.findViewById(R.id.localizacion);
            rowView.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) rowView.getTag();
		}

		final Event event = (Event) getItem(position);
        //Actualizar el evento no especial
        if (event.getC().getTimeInMillis()<Calendar.getInstance(TimeZone.getTimeZone("UTC")).getTimeInMillis() && !event.isEspecial()){
            Calendar nuevo = event.getC();

            nuevo.add(Calendar.HOUR, event.getRep());
            event.setC(nuevo);
            notifyDataSetChanged();
        }



        //Actualizar evento especial
        else if (event.isEspecial()  ){
            Calendar nuevo = new GregorianCalendar();
            nuevo.setTimeZone(TimeZone.getTimeZone("CET"));
            nuevo.set(Calendar.HOUR_OF_DAY, event.getPrimera().get(Calendar.HOUR));
            nuevo.set(Calendar.MINUTE, 0);
            nuevo.set(Calendar.SECOND, 0);
            nuevo.set(Calendar.MILLISECOND, 0);

            event.setC(nuevo);
            int i = 0;
            @SuppressLint("Recycle")
            TypedArray repes_typed = con.getResources().obtainTypedArray(R.array.repe_especiales);
            float [] repes = new float[repes_typed.length()];

            for (int k=0;k<repes.length;k++){
                repes[k]=repes_typed.getFloat(k,0);
            }
            //Algoritmo para actualizar según en que momento se esté
            while (event.getC().getTimeInMillis()<Calendar.getInstance().getTimeInMillis()){
                if (repes[i] == 4.5) {
                    nuevo.add(Calendar.HOUR, 4);
                    nuevo.add(Calendar.MINUTE, 30);
                    event.setC(nuevo);
                    i++;
                }
                else {
                    nuevo.add(Calendar.HOUR, (int) repes[i]);
                    event.setC(nuevo);
                    i++;
                    }
                }
        }
        final int icon = android.R.drawable.btn_star_big_on;

        viewHolder.mEventName.setText(event.getName());
        viewHolder.mZona.setText(event.getZona());
        String nivel = con.getString(R.string.nivel);
        viewHolder.mNivel.setText(nivel+" "+ event.getNivel());
        //Para saber que estrella pintar
        if (event.isFavorito()){
            viewHolder.mFavorito.setImageResource(icon);

        }
        else{
            viewHolder.mFavorito.setImageResource(android.R.drawable.btn_star_big_off);

        }
        //Cambiar la imagen dinámicamente
        viewHolder.mFavorito.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                event.setFavorito(!event.isFavorito());

                if (event.isFavorito()){
                    viewHolder.mFavorito.setImageResource(icon);

                }
                else{
                    viewHolder.mFavorito.setImageResource(android.R.drawable.btn_star_big_off);
                }
                Intent intent = new Intent(con, SomeService1.class) ;
                intent.putExtra("lstContact", new DataWrapper(mEvents));

                con.startService(intent);
            }
        });
        //Mostrar el tiempo restante en el formato adecuado
        long futureInMillis = event.getC().getTimeInMillis()- Calendar.getInstance(event.getC().getTimeZone()).getTimeInMillis();
        @SuppressLint("SimpleDateFormat")
        SimpleDateFormat sd =new SimpleDateFormat("HH:mm:ss");
        sd.setTimeZone(TimeZone.getTimeZone("UTC"));
        Date d = new Date(futureInMillis);
        String time = sd.format(d);
        viewHolder.mContador.setText(time);
        //Para poner en rojo el contador cuando queden menos de 15 minutos
        int quince_min = 60*15*1000;
        if (futureInMillis<quince_min){
            viewHolder.mContador.setTextColor(Color.RED);
        }
        else
            viewHolder.mContador.setTextColor(Color.BLACK);
        int img=rellenaImagen(event);
        viewHolder.mImagen.setImageResource(img);

        return rowView;
	}
    //Función para asignar la imagen que corresponda al evento
    private int rellenaImagen(Event event) {

        if (event.getNombre_imagen().equals("taidha"))
            return R.mipmap.taidha;
        else if (event.getNombre_imagen().equals("svanir"))
            return R.mipmap.svanir;
        else if (event.getNombre_imagen().equals("megadestroyer"))
            return R.mipmap.megadestroyer;
        else if (event.getNombre_imagen().equals("fire_elemental"))
            return R.mipmap.fire_elemental;
        else if (event.getNombre_imagen().equals("the_shatterer"))
            return R.mipmap.the_shatterer;
        else if (event.getNombre_imagen().equals("great_jungle"))
            return R.mipmap.great_jungle;
        else if (event.getNombre_imagen().equals("ulgoth"))
            return R.mipmap.ulgoth;
        else if (event.getNombre_imagen().equals("behemoth"))
            return R.mipmap.behemoth;
        else if (event.getNombre_imagen().equals("karka"))
            return R.mipmap.karka;
        else if (event.getNombre_imagen().equals("golem"))
            return R.mipmap.golem;
        else if (event.getNombre_imagen().equals("tequatl"))
            return R.mipmap.tequatl;
        else if (event.getNombre_imagen().equals("evolved"))
            return R.mipmap.evolved;
        else
            return R.mipmap.jormag;
    }


}
