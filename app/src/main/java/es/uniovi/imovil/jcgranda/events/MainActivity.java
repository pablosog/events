package es.uniovi.imovil.jcgranda.events;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBarActivity;

import java.util.ArrayList;


public class MainActivity extends ActionBarActivity implements  MyDialogFragment.MyDialogFragmentListener {

    @Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
        if (savedInstanceState==null) {
            FragmentManager FrgMng = getSupportFragmentManager();
            FragmentTransaction Tran = FrgMng.beginTransaction();
            Tran.add(R.id.event_list_frag, EventListFragment.newInstance());
            Tran.commit();
        }


    }

    @Override
    protected void onPause() {
        super.onPause();


    }

    @Override
    public void onSaveInstanceState(Bundle icicle) {

        super.onSaveInstanceState(icicle);

    }
    //Funcion necesaria para la comunicacion con el dialogo. No se puede comunicar directamente con el dialogo
    @Override
    public void onFinishEditDialog() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        EventListFragment fragment = (EventListFragment) fragmentManager.findFragmentById(R.id.event_list_frag);
        ArrayList<Event> cursos = fragment.getListaeventos();

        Intent intent = new Intent(this, SomeService1.class) ;
        intent.putExtra("lstContact", new DataWrapper(cursos));

        this.startService(intent);
    }


}
