package es.uniovi.imovil.jcgranda.events;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.TypedArray;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.Spinner;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;

import java.util.TimeZone;

public class EventListFragment extends Fragment implements  MyDialogFragment.MyDialogFragmentListener {
    private static final String EVENT_LIST_FILENAME = "archivoeventos";
    public static boolean activo;
    private Handler timerHandler;
    private ArrayList<Event> filtrados = new ArrayList<>();
    private int tipo_pos;
    private int nivel_pos;
    public static EventAdapter mAdapter = null;
    private SharedPreferences prefs;
    private ArrayList<Event> listaeventos;
    private int [] repe;
    private int [] levels;


    public static EventListFragment newInstance() {
        return new EventListFragment();
    }
    public ArrayList<Event> getListaeventos() {
        return listaeventos;
    }

    public EventListFragment() {
    }
    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle
                                     savedInstanceState) {
        View rootView;
        tipo_pos=0;
        nivel_pos=0;
        rootView = inflater.inflate(R.layout.event_list_fragment,
                container, false);
         ListView lvItems = (ListView) rootView.findViewById(R.id.list_view_events);

        String [] events = getResources().getStringArray(R.array.eventos);
        int [] hora = getResources().getIntArray(R.array.hora);
        int [] minuto = getResources().getIntArray(R.array.minuto);
        String [] imagenes = getResources().getStringArray(R.array.imagenes);
        levels = getResources().getIntArray(R.array.nivel);
        repe = getResources().getIntArray(R.array.repe);
        Calendar [] eventos_cal = new Calendar[hora.length];
        Calendar [] primera = new Calendar[hora.length];
        //Para actualizar el evento a la hora actual
        for (int i=0; i<minuto.length;i++) {
            Calendar date = new GregorianCalendar();
            date.set(Calendar.HOUR, hora[i]);
            date.set(Calendar.MINUTE, minuto[i]);
            date.set(Calendar.SECOND, 0);
            date.set(Calendar.MILLISECOND, 0);
            date.setTimeZone(TimeZone.getTimeZone("UTC"));
            primera[i]=(Calendar)date.clone();
            //Para que sea la hora siguiente
            while (date.getTimeInMillis()<Calendar.getInstance().getTimeInMillis()){
                date.add(Calendar.HOUR, repe[i]);
            }
            eventos_cal[i]=date;
        }
        //Sacar la lista antigua
        if (restoreList()){
            for (int i=0; i< listaeventos.size();i++){
                Event c = listaeventos.get(i);
                if (!c.isEspecial()) {
                    Calendar n=Calendar.getInstance(TimeZone.getTimeZone("UTC"));
                    n.set(Calendar.HOUR_OF_DAY, c.getPrimera().get(Calendar.HOUR));
                    n.set(Calendar.MINUTE, c.getPrimera().get(Calendar.MINUTE));
                    n.set(Calendar.SECOND,0);
                    n.set(Calendar.MILLISECOND,0);
                    c.setC(n);
                }
                while (!c.isEspecial() && c.getC().getTimeInMillis()-Calendar.getInstance(TimeZone.getTimeZone("UTC")).getTimeInMillis() < 0){

                    Calendar nuevo = c.getC();
                    nuevo.add(Calendar.HOUR, c.getRep());
                    c.setC(nuevo);
                }

            }

        }
        //Si no crearla
        else {
            listaeventos = createEventList(events, eventos_cal, imagenes, primera);
        }
        //Asignar al adaptador
        mAdapter = new EventAdapter(getActivity(), filtrados);
        lvItems.setAdapter(mAdapter);
        Spinner np = (Spinner) rootView.findViewById(R.id.niveles);
        ArrayAdapter adapter;
        adapter = ArrayAdapter.createFromResource(getActivity(), R.array.spinnerNivelesS, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        np.setAdapter(adapter);
        //Spinner para los niveles
        np.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener(){
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                nivel_pos=position;
                filtrados.clear();
                rellenaFiltrados();
                mAdapter.notifyDataSetChanged();
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        Spinner tipo = (Spinner) rootView.findViewById(R.id.tipos);
        ArrayAdapter adapter_tipos;
        adapter_tipos = ArrayAdapter.createFromResource(getActivity(), R.array.tipos, android.R.layout.simple_spinner_item);
        adapter_tipos.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        tipo.setAdapter(adapter_tipos);
        //Spinner para los tipos
        tipo.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener(){


            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                tipo_pos=position;
                filtrados.clear();
                rellenaFiltrados();
                mAdapter.notifyDataSetChanged();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {


            }
        });
        //Botón para mostrar el dialogo con el que poder elegir la hora para las alarmas
        Button dialogo = (Button) rootView.findViewById(R.id.time);
        dialogo.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                FragmentManager fm =  getFragmentManager();
                MyDialogFragment editNameDialog = new MyDialogFragment();
                editNameDialog.show(fm, "fragment_edit_name");            }
        });
         prefs =getActivity().getSharedPreferences("preferences", Context.MODE_PRIVATE);

          activo = prefs.getBoolean("active", false);
        //Checkbox para elegir si sacas alarmas o no
        final CheckBox active = (CheckBox) rootView.findViewById(R.id.avisos);
        active.setChecked(activo);
        active.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                 activo = ((CheckBox) v).isChecked();


                Intent intent = new Intent(getActivity(), SomeService1.class) ;
                intent.putExtra("lstContact", new DataWrapper(listaeventos));
                SharedPreferences.Editor prefsEditor = prefs.edit();
                prefsEditor.putBoolean("active", activo);
                prefsEditor.apply();
                getActivity().startService(intent);
            }
        });

        return rootView;
    }
    //Filtrados segun los criterios de seleccion
    private void rellenaFiltrados() {
        int[] niveles = getResources().getIntArray(R.array.spinnerNiveles);
        if (tipo_pos==0) {
            for (int i = 0; listaeventos.size() > i; i++) {
                if (listaeventos.get(i).getNivel() >= niveles[nivel_pos]) {
                    filtrados.add(listaeventos.get(i));
                }
            }
        }
        else if(tipo_pos==1){
            for (int i = 0; listaeventos.size() > i; i++) {
                if (listaeventos.get(i).getNivel() >= niveles[nivel_pos] && listaeventos.get(i).isEspecial()) {
                    filtrados.add(listaeventos.get(i));
                }
            }
        }
        else {
            for (int i = 0; listaeventos.size() > i; i++) {
                if (listaeventos.get(i).getNivel() >= niveles[nivel_pos] && !listaeventos.get(i).isEspecial()) {
                    filtrados.add(listaeventos.get(i));
                }
            }
        }
    }
    //Clase para construir la lista por primera vez
    private ArrayList<Event> createEventList(String[] names,
                                             Calendar[] cal,
                                             String[] imagenes,
                                             Calendar[] primer) {





        String [] zonas_noesp = getResources().getStringArray(R.array.zona_noesp);

        ArrayList<Event> events = new ArrayList<>(names.length);
        //Si no es especial se añade sin más
        for (int i = 0; i < names.length; i++) {

            events.add(new Event(names[i], cal[i], repe[i], levels[i], imagenes[i], i, zonas_noesp[i], primer[i]));
        }
        //Si es especial se tienen que sacar una serie de campos primeros para poder inicializarlo correctamente
        ArrayList<Event> especiales = new ArrayList<>();
        String [] nombres = getResources().getStringArray(R.array.eventos_especiales);
        String [] zonas_esp = getResources().getStringArray(R.array.zona_especiales);

        Calendar [] eventos_cal = new Calendar[nombres.length];
        String [] imagenes_esp= getResources().getStringArray(R.array.imagenes_especiales);
        @SuppressLint("Recycle") TypedArray repes_typed = getResources().obtainTypedArray(R.array.repe_especiales);
        float [] repes = new float[repes_typed.length()];
        for (int i=0;i<repes.length;i++){
            repes[i]=repes_typed.getFloat(i,0);
        }
        int[] horas_esp = getResources().getIntArray(R.array.hora_especiales);

        for (int i=0; i<horas_esp.length;i++) {

            Calendar date = new GregorianCalendar();
            date.set(Calendar.HOUR, horas_esp[i]);
            date.set(Calendar.MINUTE, 0);
            date.set(Calendar.SECOND, 0);
            date.set(Calendar.MILLISECOND, 0);
            date.setTimeZone(TimeZone.getTimeZone("CET"));
            eventos_cal[i]=date;
            Event c_esp = new Event(nombres[i], eventos_cal[i], true, 80, names.length+i, zonas_esp[i], imagenes_esp[i]);
            especiales.add(c_esp);
        }
        events.addAll(especiales);
        return events;
    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        timerHandler = new Handler();
        //Handler para actualizar el contador cada segundo
        Runnable timerRunnable = new Runnable() {
            @Override
            public void run() {

                mAdapter.notifyDataSetChanged();
                timerHandler.postDelayed(this, 1000); //run every minute
            }
        };
        timerHandler.post(timerRunnable); //run every minute*/
    }

    @Override
    public void onSaveInstanceState(Bundle icicle) {
        super.onSaveInstanceState(icicle);
        icicle.putParcelableArrayList("eventos", listaeventos);
        int mEventCount = 0;
        icicle.putInt("contador", mEventCount);
    }
    private void saveList () {
        FileOutputStream file;
        OutputStream buffer;
        ObjectOutput output = null;
        try {
            file = getActivity().openFileOutput(EVENT_LIST_FILENAME, Context.MODE_PRIVATE);
            buffer = new BufferedOutputStream(file);
            output = new ObjectOutputStream(buffer);
            output.writeObject(
                    listaeventos);
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (output != null) {
                    output.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
    @Override public void onPause() {
        super.onPause(); saveList();
        SharedPreferences.Editor prefsEditor = prefs.edit();
        prefsEditor.putBoolean("active", activo);
        prefsEditor.apply();
    }


    private boolean restoreList () {
        InputStream buffer;
        ObjectInput input = null;
        try {
            buffer = new BufferedInputStream(
                    getActivity().openFileInput(EVENT_LIST_FILENAME));
            input = new ObjectInputStream(buffer);
            listaeventos = (ArrayList<Event>)input.readObject();
            return true;
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (input != null) {
                    input.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return false;
    }


    @Override
    public void onFinishEditDialog() {
        FragmentManager fragmentManager =getFragmentManager();
        EventListFragment fragment = (EventListFragment) fragmentManager.findFragmentById(R.id.event_list_frag);
        ArrayList<Event> eventos = fragment.getListaeventos();
        Intent intent = new Intent(getActivity(), SomeService1.class) ;
        intent.putExtra("lstContact", new DataWrapper(eventos));
        getActivity().startService(intent);
    }
}
